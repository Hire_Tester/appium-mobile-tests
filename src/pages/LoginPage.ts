import BasePage from './BasePage';
import { Element } from '@wdio/sync';

class LoginPage extends BasePage {
  /**
   * define elements
   */
  get loginWithFacebookBtn(): Element {
    return $("//android.widget.Button[@resource-id='com.si.sevencup.client:id/facebookButton']");
  }

  get spinner(): Element {
    return $("//android.widget.TextView[@resource-id='android:id/message']");
  }

  get emailInput(): Element {
    return $("//android.widget.EditText[@resource-id='m_login_email']");
  }

  get passwordInput(): Element {
    return $("//android.widget.EditText[@resource-id='m_login_password']");
  }

  get loginBtn(): Element {
    return $("//android.widget.Button[@text='Log In']");
  }

  get confirmLoginBtn(): Element {
    return $("//android.widget.Button[@text='Продолжить']");
  }

  get mainContent(): Element {
    return $('~Main');
  }

  get logoutBtn(): Element {
    return $("//android.widget.ImageView[@resource-id='com.si.sevencup.client:id/exitButtonView']");
  }

  get confirmLogoutBtn(): Element {
    return $("//android.widget.Button[@text='OK']");
  }

  get errorMsg(): Element {
    return $("//android.view.View[@resource-id='login_error']/android.view.View[1]");
  }

  /**
   * define or overwrite page methods
   */
  public loginAsClient(email: string, password: string): void {
    this.loginWithFacebookBtn.click();
    this.spinner.waitForExist(undefined, true);
    this.emailInput.setValue(email);
    this.passwordInput.setValue(password);
    this.loginBtn.click();
    this.confirmLoginBtn.click();
  }

  public loginAsUnknownUser(email: string, password: string): void {
    this.loginWithFacebookBtn.click();
    this.spinner.waitForExist(undefined, true);
    this.emailInput.setValue(email);
    this.passwordInput.setValue(password);
    this.loginBtn.click();
  }

  public isLoggedIn(): boolean {
    this.mainContent.waitForExist(5000);
    return this.mainContent.isDisplayed();
  }

  public logout(): void {
    this.logoutBtn.click();
    this.confirmLogoutBtn.click();
  }

  public isError(): boolean {
    this.errorMsg.waitForExist(5000);
    return this.errorMsg.isDisplayed();
  }
}

export default new LoginPage();
