import { expect } from 'chai';
import LoginPage from '../pages/LoginPage';
import allureReporter from '@wdio/allure-reporter';
import { client, unknownUser } from '../config/constants';

describe('Login Page - Client', () => {
  it('should allow access with correct creds', () => {
    allureReporter.addStep('Authentification as client');
    allureReporter.addArgument('Email: ', client.email);
    allureReporter.addArgument('Password: ', client.password);
    LoginPage.loginAsClient(client.email, client.password);
    const status = LoginPage.isLoggedIn();
    expect(status).to.be.true;
  });

  after(() => {
    allureReporter.addStep('Logout from the application');
    LoginPage.logout();
  });
});

describe('Login Page - Unknown User', () => {
  it('should deny access with wrong creds', () => {
    allureReporter.addStep('Authentification as unregistered user');
    allureReporter.addArgument('Email: ', unknownUser.email);
    allureReporter.addArgument('Password: ', unknownUser.password);
    LoginPage.loginAsUnknownUser(unknownUser.email, unknownUser.password);
    const status = LoginPage.isError();
    expect(status).to.be.true;
  });
});
