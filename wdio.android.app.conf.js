/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('deepmerge');
const wdioConf = require('./wdio.shared.conf.js');

// have main config file as default but overwrite environment specific information
exports.config = merge(wdioConf.config, {
  host: '0.0.0.0',
  port: 4725,
  path: '/wd/hub',
  capabilities: [
    {
      // The defaults you need to have in your config
      platformName: 'Android',
      maxInstances: 1,
      // For W3C the appium capabilities need to have an extension prefix
      // http://appium.io/docs/en/writing-running-appium/caps/
      // This is `appium:` for all Appium Capabilities which can be found here
      'appium:deviceName': 'Pixel 2',
      'appium:platformVersion': '10.0',
      'appium:orientation': 'PORTRAIT',
      // `automationName` will be mandatory, see
      // https://github.com/appium/appium/releases/tag/v1.13.0
      'appium:automationName': 'UiAutomator2',
      // The path to the app
      'appium:app': '/home/user/projects/7cups-appium-mobile-tests/app/SevenCups_Client_1.1.8(118).apk',
      // Read the reset strategies very well, they differ per platform, see
      // http://appium.io/docs/en/writing-running-appium/other/reset-strategies/
      'appium:noReset': true,
      'appium:newCommandTimeout': 240,
    },
  ],
});
