# appium-mobile-tests

<p align="center">
    <img src="./readme.png">
</p>

<p align="center">
   <i><strong>Test framework for automating mobile apps with appium using webdriverio &amp; typescript!</strong></i>
<p>

---

### <p align="center"> [About](#about) **|** [Getting Started](#getting-started) **|** [Installation](#installation) **|** [Writing Tests](#writing-tests) **|** [Page Objects](#page-objects) **|** [Reports](#reports)</p>

## About

This is sample code for running Appium・Android tests on [Docker-Android](https://github.com/budtmo/docker-android).

It uses [Appium](http://appium.io/), [WebdriverIO](https://webdriver.io/), [TypeScript](https://www.typescriptlang.org/), and [Mocha](https://mochajs.org/index.html)・[Chai](https://www.chaijs.com/).

## Getting Started

### Pre-requisites

1. [NodeJS](https://nodejs.org/en/download/) installed globally in the system.

2.  JAVA(jdk) installed in the system.

3. Andriod(sdk) installed in the system.

4. Set **JAVA_HOME** & **ANDROID_HOME** paths correctly in the system.

5. Installing [Appium](https://github.com/webdriverio/appium-boilerplate/blob/master/docs/APPIUM.md) on a local machine

6. Setting up Android on a local machine ([check here](./ANDROID_SETUP.md#setting-up-android-and-iOS-on-a-local-machine))

**Tip:** Install `npm install -g appium-doctor` and run it from the command-line which checks if your java jdk and android sdk paths are set correctly or not.

## Installation

### Setup Scripts

* Clone the repository into a folder
* Go inside the folder and run following command from terminal/command prompt
```
npm install
```
* All the dependencies from package.json and typescript typings would be installed in node_modules folder.

### Run Tests

* First step is to start the `appium` server and run `android emulator`.

Next step is to execute the config files. This project has 3 config files -

* [wdio.shared.conf.js](./wdio.shared.conf.js) - This shared config holds all the defaults so the Android configs only need to hold the capabilities that are needed for running on Android.
* [wdio.android.app.conf.js](./wdio.android.app.conf.js) - This android config holds all capabilities for Android emulator.
* [wdio.ci.android.app.conf.js](./wdio.ci.android.app.conf.js) - This continuous integration config holds all necessary for running tests remotely on CI server.

The node command to run Native app tests of this project is -

```
npm run test
```
The above command which is set in `package.json` internally calls the WebdriverIO's binary `wdio ./wdio.android.app.conf.js`  and runs the app config file.

## Writing Tests

``` JS
import { expect } from 'chai';
import LoginPage from '../pages/LoginPage';
import allureReporter from '@wdio/allure-reporter';
import { client, unknownUser } from '../config/constants';

describe('Login Page - Client', () => {
  it('should allow access with correct creds', () => {
    allureReporter.addStep('Authentification as client');
    allureReporter.addArgument('Email: ', client.email);
    allureReporter.addArgument('Password: ', client.password);
    LoginPage.loginAsClient(client.email, client.password);
    const status = LoginPage.isLoggedIn();
    expect(status).to.be.true;
  });

  after(() => {
    allureReporter.addStep('Logout from the application');
    LoginPage.logout();
  });
});
```

## Page Objects

This framework is strictly written using page-object design pattern.

``` JS
import BasePage from './BasePage';
import { Element } from '@wdio/sync';

class LoginPage extends BasePage {
  /**
   * define elements
   */
  get loginWithFacebookBtn(): Element {
    return $("//android.widget.Button[@resource-id='com.si.sevencup.client:id/facebookButton']");
  }

  get spinner(): Element {
    return $("//android.widget.TextView[@resource-id='android:id/message']");
  }

  get emailInput(): Element {
    return $("//android.widget.EditText[@resource-id='m_login_email']");
  }

  get passwordInput(): Element {
    return $("//android.widget.EditText[@resource-id='m_login_password']");
  }

  get loginBtn(): Element {
    return $("//android.widget.Button[@text='Log In']");
  }

  get confirmLoginBtn(): Element {
    return $("//android.widget.Button[@text='Продолжить']");
  }

  get mainContent(): Element {
    return $('~Main');
  }

  get logoutBtn(): Element {
    return $("//android.widget.ImageView[@resource-id='com.si.sevencup.client:id/exitButtonView']");
  }

  get confirmLogoutBtn(): Element {
    return $("//android.widget.Button[@text='OK']");
  }

  get errorMsg(): Element {
    return $("//android.view.View[@resource-id='login_error']/android.view.View[1]");
  }

  /**
   * define or overwrite page methods
   */
  public loginAsClient(email: string, password: string): void {
    this.loginWithFacebookBtn.click();
    this.spinner.waitForExist(undefined, true);
    this.emailInput.setValue(email);
    this.passwordInput.setValue(password);
    this.loginBtn.click();
    this.confirmLoginBtn.click();
  }

  public loginAsUnknownUser(email: string, password: string): void {
    this.loginWithFacebookBtn.click();
    this.spinner.waitForExist(undefined, true);
    this.emailInput.setValue(email);
    this.passwordInput.setValue(password);
    this.loginBtn.click();
  }

  public isLoggedIn(): boolean {
    this.mainContent.waitForExist(5000);
    return this.mainContent.isDisplayed();
  }

  public logout(): void {
    this.logoutBtn.click();
    this.confirmLogoutBtn.click();
  }

  public isError(): boolean {
    this.errorMsg.waitForExist(5000);
    return this.errorMsg.isDisplayed();
  }
}

export default new LoginPage();
```

## Reports

### Allure Reporter
> A WebdriverIO reporter plugin to create [Allure Test Reports](https://docs.qameta.io/allure/)

<p align="center">
  <img src="http://i.imgur.com/NvPgkY7.png">
</p>

#### Installation

``` JS
npm i -D @wdio/allure-reporter@5.16.0
```

** Allure doesn't correctly report **'passing'** tests starting with *@wdio/allure-reporter version* **5.16.10**

#### Configuration

``` JS
// wdio.conf.js
reporters: [
  'spec',
  ['allure', {
    outputDir: 'allure-results',
    disableWebdriverStepsReporting: true,
  }],
],
```

#### Usage
Allure API can be accessed using:

``` JS
import allureReporter from "@wdio/allure-reporter";
```

> [Supported Allure API](https://webdriver.io/docs/allure-reporter.html#supported-allure-api)

#### Displaying the report

The results can be consumed by any of the [reporting tools](https://docs.qameta.io/allure/#_reporting) offered by Allure. For example:</br>
#### Command-line

Install the [Allure command-line tool](https://www.npmjs.com/package/allure-commandline), and run the next command:

``` JS
npm run report
```

This will generate a report (by default in **./allure-report**), and open it in your browser.

####  Add Screenshots
Screenshots can be attached to the report by using the **takeScreenshot** function from WebDriverIO in **afterStep** hook. First set *disableWebdriverScreenshotsReporting: false* in reporter options, then add in **afterStep** hook

``` JS
// wdio.conf.js
afterTest: function (test) {
  if (test.error !== undefined) {
    browser.takeScreenshot();
  }
},
```

### JUnit Reporter
> A WebdriverIO reporter that creates Gitlab CI compatible XML based reports.

This reporter will output a report for each runner, so in turn you will receive an XML report for each spec file. The report is placed in **e2e/reports/junit-report** folder.

#### Installation

``` JS
npm i -D @wdio/junit-reporter
```

#### Configuration

``` JS
reporters: [
  //..
  ['junit', {
    outputDir: './',
    outputFileFormat: () => 'e2e-results.xml'
  }]
  //..
],
```
